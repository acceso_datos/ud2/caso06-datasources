package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConexionPoolingHikariCP {
	private static HikariDataSource poolDataSource = null;

	public static synchronized Connection getConexion() throws SQLException {		
		if (poolDataSource == null) {			
			HikariConfig config = new HikariConfig();
			config.setJdbcUrl("jdbc:mariadb://192.168.56.103:3306/empresa");
			config.setUsername("batoi");
			config.setPassword("1234");
			config.setMaximumPoolSize(10); // Número máximo de conexiones en el pool

			// Configuración avanzada opcional
			config.setMinimumIdle(2); 		// Conexiones mínimas inactivas en el pool
			config.setIdleTimeout(30000); 	// Tiempo de espera antes de cerrar conexiones inactivas (en ms)
			config.setMaxLifetime(1800000); // Tiempo máximo de vida de una conexión (en ms)
			config.setConnectionTimeout(30000); // Tiempo máximo para esperar una conexión (en ms)
			poolDataSource = new HikariDataSource(config);
			System.out.println(Thread.currentThread().getName()+" crea el poolDataSource");
		}

		return poolDataSource.getConnection();
	}

	public static void liberarConexion(Connection con) throws SQLException {
		if (con != null) {
			con.close();
		}
	}
	
	// Hook en la JVM para asegurar que el pool se cierra correctamente al cerrar el programa.
	static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (poolDataSource != null) {
                poolDataSource.close();
                poolDataSource = null;
                System.out.println("HikariCP pool cerrado.");
            }
        }));
    }

}

// Información extra:
// setMinimumIdle: configura el número mínimo de conexiones inactivas que deben estar disponibles en el pool en todo momento. 
	// Con setMinimumIdle(2), HikariCP mantendrá siempre al menos 2 conexiones inactivas listas en el pool.
// setIdleTimeout: define el tiempo máximo (en milisegundos) que una conexión puede permanecer inactiva en el pool antes de ser cerrada. 
	// Con setIdleTimeout(30000), cualquier conexión que esté inactiva por más de 30 segundos será cerrada automáticamente por el pool.
// setMaxLifetime: define el tiempo máximo de vida útil de una conexión en el pool. 
	// Con setMaxLifetime(1800000), cada conexión en el pool se cerrará automáticamente después de 30 minutos (1,800,000 milisegundos), independientemente de su estado de uso
// setConnectionTimeout: especifica el tiempo máximo (en milisegundos) que un hilo debe esperar para obtener una conexión del pool antes de que se lance una excepción SQLTimeoutException. 
	// Con setConnectionTimeout(30000), el hilo que solicita una conexión esperará un máximo de 30 segundos.
// public static synchronized Connection getConexion(): método sincronizado. Una mejor solución sería bloque sincronizado:
/* public static Connection getConexion() throws SQLException {
    if (poolDataSource == null) {
        synchronized (ConexionPoolingHikariCP.class) {
            if (poolDataSource == null) {
                HikariConfig config = new HikariConfig();
                config.setJdbcUrl("jdbc:mariadb://192.168.56.103:3306/empresa");
                config.setUsername("batoi");
                config.setPassword("1234");
                config.setMaximumPoolSize(3);
                poolDataSource = new HikariDataSource(config);
            }
        }
    }
    return poolDataSource.getConnection();
} */
//Primera comprobación ¿poolDataSource == null? (fuera del bloque synchronized):
//Evita el costo de sincronización innecesaria si el objeto ya está inicializado.
//Segunda comprobación (dentro del bloque synchronized):
//Garantiza que solo un hilo inicialice el objeto. Este paso es necesario porque otro hilo podrían haber inicializado 
// poolDataSource mientras el primer hilo estaba esperando entrar al bloque sincronizado.

