package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbPoolDataSource;

public class ConexionDSPool {
	private static MariaDbPoolDataSource pds;
//	private static PGConnectionPoolDataSource pds;
	private final static String IP = "192.168.56.103";
	private final static String DB = "empresa";

	public static Connection getConexion() throws SQLException {
		if (pds == null) {
			synchronized (ConexionDSPool.class) {
			pds = new MariaDbPoolDataSource("jdbc:mariadb://" + IP + ":3306" + "/" + DB);
			//pds.setUrl("jdbc:mariadb://" + IP + ":3306" + "/" + DB);
//			pds.setServerName(IP);
//			pds.setDatabaseName(DB);
//			pds.setPortNumber(3306)
			pds.setUser("batoi");
			pds.setPassword("1234");
			// pds.setMaxPoolSize(3); // no disponible (obsoleto). disponible en versiones anteriores (ejemplo driver 2.7.4)
			
//			pds = new PGConnectionPoolDataSource();
//			pds.setUrl("jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=" + DB);
////			pds.setDatabaseName("batoi");
////			pds.setCurrentSchema(DB);
//			pds.setUser("batoi");
//			pds.setPassword("1234)");
//			pds.setMaxPoolSize(3); // no disponible	(obsoleto)	
			// JDBC is an API and specification, it will never provide pooling itself.
			// The reason you need to use third-party libraries that provide a 
			// JDBC DataSource is simple: it is hard to do connection pooling correct 
			// and performant. A lot of JDBC drivers initially had their own connection pool implementation, but most are (were) either buggy or don't perform well (or both). 
			// The market has produced better alternatives.
			// https://stackoverflow.com/questions/47334601/how-to-limit-postgresql-jdbc-pool-connections-amount
			
			// Mejor usar implementaciones de terceros: Apache Commons DBCP, HikariCP o c3p0
			}
		}
		return pds.getConnection();
	}

	public static void liberarConexion(Connection con) throws SQLException {
		if (con != null) {
			con.close();
		}
	}	
	
	// hook en la JVM para asegurar que el pool se cierra correctamente al cerrar el programa.
	static {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (pds != null) {
            	pds.close();
            	pds = null;
                System.out.println("Pool cerrado.");
            }
        }));
	}


}
