package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author sergio
 */
public class App {

	public static void main(String[] args) {

		ejemploDataSource();
		// Simulamos varios hilos que hacen peticiones concurrentes al pool de conexiones
        for (int i = 1; i <= 3; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ejemploPooling();
                }
            }, "Hilo" + i).start();
        }

	}

	private static void ejemploDataSource() {
		try {
			Connection con = ConexionDS.getConexion();
			System.out.println("DataSource - Conexión ok: " + con);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				ConexionDS.cerrar();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}

	}

		   
	private static void ejemploPooling() {
		Connection con1, con2, con3, con4;

        System.out.println(Thread.currentThread().getName() + " - ----------------------------------------------");
        System.out.println(Thread.currentThread().getName() + " - Pooling de conexiones con HikariCP");

        // Ejemplo con Pool: realizamos varias conexiones y liberamos.
        try {
            con1 = ConexionPoolingHikariCP.getConexion();
            System.out.println(Thread.currentThread().getName() + " - Conexion1 ok: " + con1);

            con2 = ConexionPoolingHikariCP.getConexion();
            System.out.println(Thread.currentThread().getName() + " - Conexion2 ok: " + con2);

            ConexionPoolingHikariCP.liberarConexion(con1);

            con3 = ConexionPoolingHikariCP.getConexion();
            System.out.println(Thread.currentThread().getName() + " - Conexion3 ok: " + con3 + " (reutilizada)");

            con4 = ConexionPoolingHikariCP.getConexion();
            System.out.println(Thread.currentThread().getName() + " - Conexion4 ok: " + con4);

            ConexionPoolingHikariCP.liberarConexion(con1);
            ConexionPoolingHikariCP.liberarConexion(con2);
            ConexionPoolingHikariCP.liberarConexion(con3);
            ConexionPoolingHikariCP.liberarConexion(con4);
        } catch (SQLException e) {
            System.out.println(Thread.currentThread().getName() + " - Error: " + e.getMessage());
        }
    }
	

	// No funciona en las ultimas versiones -> Usar otras
	private static void ejemploPool0() {
		Connection con1, con2, con3, con4;

		System.out.println("----------------------------------------------");
		System.out.println("Pooling de conexiones...");

		// Ejemplo con Pool: realizamos varias conexiones y liberamos.
		try {
			con1 = ConexionDSPool.getConexion();
			System.out.println("Conexion1 ok: " + con1);

			con2 = ConexionDSPool.getConexion();
			System.out.println("Conexion2 ok: " + con2);

			// ConexionDSPool.liberarConexion(con1);

			con3 = ConexionDSPool.getConexion();
			System.out.println("Conexion3 ok: " + con3 + " (reutilizada)");

			con4 = ConexionDSPool.getConexion();
			System.out.println("Conexion4 ok: " + con4);

			ConexionDSPool.liberarConexion(con1);
			ConexionDSPool.liberarConexion(con2);
			ConexionDSPool.liberarConexion(con3);
			ConexionDSPool.liberarConexion(con4);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}

}
